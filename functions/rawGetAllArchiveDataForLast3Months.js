var rawArchiveDataOfProject;
var numberOfMonthsBack = 3;
var startingDate = Time.sDateToUTCTime(new Date().format("yyyy-mm"));
for (var i = 0; i < numberOfMonthsBack; i++){
	$.ajax({
	  type: 'GET',
	  url: "https://i.doit.im/api/tasks/archive/monthly/"+ startingDate +"?_=" + Date.now(),
	  success: function(data) {
	  		for(var actualTaskIndex = 0; actualTaskIndex < data.entities.length ; actualTaskIndex++){
	  			$.ajax({
				  type: 'GET',
				  url: "https://i.doit.im/api/subtasks/" + data.entities[actualTaskIndex].uuid + "?_=" + Date.now(),
				  success: function(subtasksData) {
						data.entities[actualTaskIndex].subtasks = subtasksData;
				    },
				  async:false
				});
	  		}
	  		rawArchiveDataOfProject["month_" + new Date(startingDate).format("yyyy-mm")] = data;
	    },
	  async:false
	});
	startingDate = Time.sDateToUTCTime(GTD.getFormatMonth(new Date(startingDate).format("yyyy-mm-dd"), "prev")[1]);
}