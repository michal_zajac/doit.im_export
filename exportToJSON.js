//Please set the numberOfMonthsBack to number of months back you want to export your archive
var numberOfMonthsBack = 3;
var resourcesData;
var JSONArrayOfRawListDataOfAllProjects;
var finalDataJSON = new Object();

function getListWithSubtasks(urlString, tasksAssignedToProject){
	var result = new Array();
	$.ajax({
	  type: 'GET',
	  url: urlString,
	  success: function(data) {
	  		for(var actualTaskIndex = 0; actualTaskIndex < data.entities.length ; actualTaskIndex++){
	  			if(!tasksAssignedToProject){
	  				if("project" in data.entities[actualTaskIndex]) continue;
	  			}
	  			$.ajax({
				  type: 'GET',
				  url: "https://i.doit.im/api/subtasks/" + data.entities[actualTaskIndex].uuid,
				  success: function(subtasksData) {
						data.entities[actualTaskIndex].subtasks = subtasksData.entities;
				    },
				  async:false
				});
				result.push(data.entities[actualTaskIndex]);
	  		}
	    },
	  async:false
	});
	return result;
}

$.ajax({
	  type: 'GET',
	  url: "https://i.doit.im/api/resources_init?_=" + Date.now(),
	  success: function(data) {
	  		console.log("This may take more than 15 minutes. Please wait... ");
			resourcesData = data;

			finalDataJSON.projects = new Array();
			console.log("...exporting projects:");
			for(var i = 0; i < resourcesData.resources.projects.length; i++){
				var project = new Object();
				console.log("Exporting: " + resourcesData.resources.projects[i].name);
				project.project = resourcesData.resources.projects[i];
				project.incomplete = getListWithSubtasks("https://i.doit.im/api/tasks/project/" + resourcesData.resources.projects[i].uuid, true);
				project.completed = getListWithSubtasks("https://i.doit.im/api/tasks/project/complete/" + resourcesData.resources.projects[i].uuid, true);
				project.archived = getListWithSubtasks("https://i.doit.im/api/tasks/project/archive/" + resourcesData.resources.projects[i].uuid, true);
				finalDataJSON.projects.push(project);
			}

			console.log("Exporting: Inbox");
			finalDataJSON.inbox = getListWithSubtasks("https://i.doit.im/api/tasks/inbox", false);
			console.log("Exporting: Today");
			finalDataJSON.today = getListWithSubtasks("https://i.doit.im/api/tasks/today", false);
			console.log("Exporting: Next");
			finalDataJSON.next = getListWithSubtasks("https://i.doit.im/api/tasks/next", false);
			console.log("Exporting: Scheduled");
			finalDataJSON.scheduled = getListWithSubtasks("https://i.doit.im/api/tasks/scheduled", false);
			console.log("Exporting: Someday");
			finalDataJSON.someday = getListWithSubtasks("https://i.doit.im/api/tasks/someday", false);
			console.log("Exporting: Waiting");
			finalDataJSON.waiting = getListWithSubtasks("https://i.doit.im/api/tasks/waiting", false);
			console.log("Exporting: Goals");
			finalDataJSON.goals = new Array();
			for(var i = 0; i < resourcesData.resources.goals.length; i++){
				var goal = new Object();
				goal.goal = resourcesData.resources.goals[i];
				if(resourcesData.resources.goals[i].trashed == 0){
					goal.incomplete = getListWithSubtasks("https://i.doit.im/api/tasks/goal/" + resourcesData.resources.goals[i].uuid, true);
					goal.completed = getListWithSubtasks("https://i.doit.im/api/tasks/goal/complete/" + resourcesData.resources.goals[i].uuid, true);
					goal.archived = getListWithSubtasks("https://i.doit.im/api/tasks/goal/archive/" + resourcesData.resources.goals[i].uuid, true);
					finalDataJSON.goals.push(goal);
				}
			}
			finalDataJSON.completed = getListWithSubtasks("https://i.doit.im/api/tasks/completed", false);
			finalDataJSON.archived = new Object();
			var startingDate = Time.sDateToUTCTime(new Date().format("yyyy-mm"));
			for (var i = 0; i < numberOfMonthsBack; i++){
				var readableStartingDate = "month_" + new Date(startingDate).format("yyyy-mm");
				finalDataJSON.archived[readableStartingDate] = getListWithSubtasks("https://i.doit.im/api/tasks/archive/monthly/"+ startingDate, false);
				startingDate = Time.sDateToUTCTime(GTD.getFormatMonth(new Date(startingDate).format("yyyy-mm-dd"), "prev")[1]);
			}

			finalDataJSON.contexts = Doit.contexts;
	    },
	  async:false
	});
console.log("Export completed.");
copy(finalDataJSON);

