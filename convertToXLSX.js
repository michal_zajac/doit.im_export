function getTableID(tableName){
	var tableId = window.btoa(unescape(encodeURIComponent(tableName)));
	tableId = tableId.replace(/=/g, '');
	return "table" + tableId;
}

function showTable(data){
	$("#task_quick_add").append('<div id="zajoTable" style="height:800px;overflow:auto;">\
	</div>"');

	var tableName = "Inbox";
	constructTabe(tableName);
	for(var i = 0; i < data.inbox.length; i++){
		addTaskToTable(tableName, data.inbox[i]);
	}

	tableName = "Today";
	constructTabe(tableName);
	for(var i = 0; i < data.today.length; i++){
		addTaskToTable(tableName, data.today[i]);
	}

	tableName = "Next";
	constructTabe(tableName);
	for(var i = 0; i < data.next.length; i++){
		addTaskToTable(tableName, data.next[i]);
	}

	tableName = "Scheduled";
	constructTabe(tableName);
	for(var i = 0; i < data.scheduled.length; i++){
		addTaskToTable(tableName, data.scheduled[i]);
	}

	tableName = "Someday";
	constructTabe(tableName);
	for(var i = 0; i < data.someday.length; i++){
		addTaskToTable(tableName, data.someday[i]);
	}

	tableName = "Waiting";
	constructTabe(tableName);
	for(var i = 0; i < data.waiting.length; i++){
		addTaskToTable(tableName, data.waiting[i]);
	}

	for(var projectIndex = 0; projectIndex < data.projects.length; projectIndex++){
		constructTabe(data.projects[projectIndex].project.name);
		var row = $("<tr />");
		$("#" + getTableID(data.projects[projectIndex].project.name)).append(row);
		row.append($("<td>" + data.projects[projectIndex].project.name + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + data.projects[projectIndex].project.notes + "</td>"));
		row.append($("<td>" + new Date(data.projects[projectIndex].project.created).format("yyyy-mm-dd") + "</td>"));
		row.append($("<td>" + new Date(data.projects[projectIndex].project.updated).format("yyyy-mm-dd") + "</td>"));

		tableName = data.projects[projectIndex].project.name + "_Incomplete";
		constructTabe(tableName);
		for(var i = 0; i < data.projects[projectIndex].incomplete.length; i++){
			addTaskToTable(tableName, data.projects[projectIndex].incomplete[i]);
		}

		tableName = data.projects[projectIndex].project.name + "_Completed";
		constructTabe(tableName);
		for(var i = 0; i < data.projects[projectIndex].completed.length; i++){
			addTaskToTable(tableName, data.projects[projectIndex].completed[i]);
		}

		tableName = data.projects[projectIndex].project.name + "_Archived";
		constructTabe(tableName);
		for(var i = 0; i < data.projects[projectIndex].archived.length; i++){
			addTaskToTable(tableName, data.projects[projectIndex].archived[i]);
		}
	}

	for(var goalIndex = 0; goalIndex < data.goals.length; goalIndex++){
		constructTabe(data.goals[goalIndex].goal.name);
		var row = $("<tr />");
		$("#" + getTableID(data.goals[goalIndex].goal.name)).append(row);
		row.append($("<td>" + data.goals[goalIndex].goal.name + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + "---" + "</td>"));
		row.append($("<td>" + data.goals[goalIndex].goal.notes + "</td>"));
		row.append($("<td>" + new Date(data.goals[goalIndex].goal.created).format("yyyy-mm-dd") + "</td>"));
		row.append($("<td>" + new Date(data.goals[goalIndex].goal.updated).format("yyyy-mm-dd") + "</td>"));

		tableName = data.goals[goalIndex].goal.name + "_Goal_Incomplete";
		constructTabe(tableName);
		for(var i = 0; i < data.goals[goalIndex].incomplete.length; i++){
			addTaskToTable(tableName, data.goals[goalIndex].incomplete[i]);
		}

		tableName = data.goals[goalIndex].goal.name + "_Goal_Completed";
		constructTabe(tableName);
		for(var i = 0; i < data.goals[goalIndex].completed.length; i++){
			addTaskToTable(tableName, data.goals[goalIndex].completed[i]);
		}

		tableName = data.goals[goalIndex].goal.name + "_Goal_Archived";
		constructTabe(tableName);
		for(var i = 0; i < data.goals[goalIndex].archived.length; i++){
			addTaskToTable(tableName, data.goals[goalIndex].archived[i]);
		}
	}

	$('tr:nth-child(even)').css("background", "#f2f2f2");
	$('td').css("border", "1px solid black");
}

function getTaskTableHeader(){
	var taskTableHeader = "<tr>\
        <th>Title</th>\
        <th>Priority</th>\
        <th>Attribute</th>\
        <th>Project</th>\
        <th>Context</th>\
        <th>Tags</th>\
        <th>Notes</th>\
        <th>Time created</th>\
        <th>Time updated</th>\
    </tr>";
    return taskTableHeader;
}

function constructTabe(tableName){
	$("#zajoTable").append('<BR> <BR> <h1>' + tableName + '</h1>\
		<table id="' + getTableID(tableName) + '"> ' + 
		getTaskTableHeader() + 
	'</table>');
}

function addTaskToTable(tableName, task){
	var row = $("<tr />");
	$("#"+ getTableID(tableName)).append(row);
	row.append($("<td>" + task.title + "</td>"));
	row.append($("<td>" + task.priority + "</td>"));
	row.append($("<td>" + task.attribute + "</td>"));
	row.append($("<td>" + projLookup(task.project) + "</td>"));
	row.append($("<td>" + contextLookup(task.context) + "</td>"));

	var tags = "";
	if(!(typeof task.tags === 'undefined')){
		for(var tagIndex = 0; tagIndex < task.tags.length; tagIndex++){
			tags += task.tags[tagIndex];
			if(tagIndex != task.tags.length - 1 ) tags += ", ";
		}
	} 
	row.append($("<td>" + tags + "</td>"));

	var note = "";
	if(!(typeof task.notes === 'undefined')){
		note = task.notes;
	}
	row.append($("<td>" + note + "</td>"));

	row.append($("<td>" + new Date(task.created).format("yyyy-mm-dd") + "</td>"));
	row.append($("<td>" + new Date(task.updated).format("yyyy-mm-dd") + "</td>"));
}

function convertToXLSX(jsonObject){
	tableToExcel("zajoTable", "zajoTable");
}

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()

function projLookup(projId) {
	var projects = Doit.projects;
    for(var i=0; i< projects.length; i++) {
        if (projects[i].uuid == projId) return projects[i].name;
    }
    return "---";
}

function contextLookup(ctxId) {
	var arr = Doit.contexts;
    for(var i=0; i< arr.length; i++) {
        if (arr[i].uuid == ctxId) return arr[i].name;
    }
    return "---";
}
